import os
import sys
from app import App
import logging
import argparse


def parse_cmdargs(args):
    parser = argparse.ArgumentParser(
        description='Generate weekly digest using Google Calendar v3 API.'
    )

    parser.add_argument(
        'imgdir',
        help='Name of directory that stores all images.')
    parser.add_argument(
        '-o', '--output-file', dest='output',
        type=str,
        help='Name of file to output finished image.',
        default='out.png')
    parser.add_argument(
        '-f', '--force-refresh-cache', dest='refresh_cache',
        action='store_true',
        help='Force stored cache to refresh, calling the API.')
    parser.add_argument(
        '-c', '--credentials', dest='creds_file',
        type=str,
        help='Name of file that stores Google API credentials.',
        default='credentials.json')
    parser.add_argument(
        '-p', '--token-pickle', dest='token_file',
        type=str,
        help='Name of file that stores pickled OAuth object.',
        default='token.pkl')

    return parser.parse_args(args)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    a = App(parse_cmdargs(sys.argv[1:]), os.environ)
    a.run()
