# gCal wallpaper generator

Are you tired of unlocking your phone and tapping the "Calendar" app to look at
your schedule? Well we have a solution for you!

With the gCal wallpaper generator, you no longer have to check your calendar
for upcoming events. Instead, they can be displayed as the background of your
desktop!

## Requirements

This thing runs on Python and Pipenv.

```
pipenv install
```

Make sure you fill out a `.env` file with your variables. Also make sure that
you have downloaded your credentials as a `.json`. Head to Google Cloud Console
and download OAuth2 credentials for Calendar API v3 and rename it to
`credentials.json`.

```sh
# file: .env
CALENDAR_ID=primary
```

## Usage

The main Python program generates the wallpaper given a list of images.

```
# Generate by picking a random wallpaper from a directory
pipenv run gen <wallpaper-directory> image.png

# A short script that generates the wallpaper and puts it as your background
# image using `feh`
./make-bg.sh
```

You can use the script `./set_wallpaper.sh` to both generate and set the image
as wallpaper automatically. It uses `feh`.

```
./set_wallpaper.sh /my/wallpaper/directory/
```

## Screenshots

![schedule](img/screenshot0.png)
