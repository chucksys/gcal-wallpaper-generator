from app.calendar import GoogleCalendar
from app.drawer import ScheduleDrawer
from app.cache import FileCache
from glob import glob
import random
import os.path
from mimetypes import MimeTypes


def get_random_image(dirname: str) -> str:
    filenames_to_try = glob(os.path.join(dirname, '*'))
    random.shuffle(filenames_to_try)
    m = MimeTypes()
    for name in filenames_to_try:
        t, enc = m.guess_type(name)
        if t is not None and t.startswith('image/'):
            return name
    raise ValueError('Cannot find an image file')


class App:
    def __init__(self, args, env):
        self.cache = FileCache(env)
        self.cal = GoogleCalendar(args, env, self.cache)
        self.args = args

    def run(self):
        # Get and load random image
        if self.cache.imagename is None or self.args.refresh_cache:
            self.cache.imagename = get_random_image(self.args.imgdir)
        drawer = ScheduleDrawer(self.cache.imagename, (1920, 1080), self.cal)

        # Draw the events onto the image
        drawer.draw_schedule()

        # Draw image onto output file
        drawer.save(self.args.output)

        # Save everything to cache
        self.cache.cache()
