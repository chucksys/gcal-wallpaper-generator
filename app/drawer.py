from PIL import Image, ImageDraw, ImageFont
from app.knuthplass import GreedyFormatter
from app.calendar import GoogleCalendar
from datetime import datetime, timedelta
from typing import Tuple


DAYS = ['Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat', 'Sun']
MONTHS = ['WUT', 'January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'September', 'October', 'November', 'December']


def duration_to_hours(duration: timedelta) -> float:
    return duration.total_seconds() / 3600


def percent_of_day(dt: datetime) -> float:
    return (dt.hour + dt.minute / 60 + dt.second / 3600) / 24


def get_time(evt_inner) -> datetime:
    if 'dateTime' in evt_inner:
        return datetime.fromisoformat(evt_inner['dateTime'])
    else:
        return datetime.fromisoformat(evt_inner['date'])


class ScheduleDrawer:
    def __init__(self, imagefile: str, size: Tuple[int, int],
                 cal: GoogleCalendar):
        self.cal = cal
        self.size = size
        self.im = Image.open(imagefile).resize(size).convert('RGBA')
        self.fontsize = 12
        self.bigfontsize = size[1] // 5
        self.fontcolor = (230, 230, 230, 255)
        self.bigfontcolor = (200, 200, 200, 150)
        self.font = ImageFont.truetype(
            'Ubuntu Mono Nerd Font Complete.ttf',
            self.fontsize)
        self.bigfont = ImageFont.truetype(
            'Ubuntu Mono Nerd Font Complete.ttf',
            self.bigfontsize)
        self.sepcolor = (170, 170, 170, 255)
        self.mecolor = (200, 10, 5, 255)
        self.evtcolor = (3, 135, 199, 255)
        self.fulldaycolor = (37, 46, 95, 255)

    def draw_schedule(self):
        # We create another image to draw the schedule on, and then at the end
        # we draw the entire image onto the original image.
        panel_size = (int(self.size[0] * 0.75), int(self.size[1] * 0.833))
        panel_bg = (0, 0, 0, 200)
        self.panel = Image.new('RGBA', panel_size, panel_bg)
        draw = ImageDraw.Draw(self.panel)
        now = datetime.now()

        # Draw the month in huge text, overlayed atop the entire calendar
        month = MONTHS[now.month]
        w, h = draw.textsize(month, font=self.bigfont)
        draw.text(((panel_size[0] - w) // 2,
                   (panel_size[1] - h) // 2),
                  month,
                  font=self.bigfont,
                  fill=self.bigfontcolor)

        # Column with the hours 24-hour system
        # Treat it as if we have 26 rows instead of 24 because we want an extra
        # to put the days of the week and the dates.
        self.vpad = (panel_size[1] - 26 * self.fontsize) // 27
        self.hpad = 3
        self.colw = panel_size[0] // 8 - 7
        self.total_space = 24 * (self.vpad + self.fontsize)
        for h in range(25):
            text = '{:02}:00'.format(h)
            texty = self.vpad + h * (self.vpad + self.fontsize)
            w = draw.textsize(text, font=self.font)[0]
            draw.text((self.colw - self.hpad * 2 - w, texty),
                      text, align='left',
                      font=self.font, fill=self.fontcolor)
            draw.line((self.colw - self.hpad,
                       texty + self.fontsize // 2,
                       panel_size[0] - self.colw // 3,
                       texty + self.fontsize // 2),
                      fill=self.sepcolor,
                      width=1)

        # Put the columns in. All 8 of them. With the weekdays on the right
        # Make sure that the text is centered
        for col in range(7):
            draw.line((self.colw + col * (self.colw + 1),
                       self.vpad // 2,
                       self.colw + col * (self.colw + 1),
                       panel_size[1] - self.vpad * 2),
                      fill=self.sepcolor,
                      width=1)

            # Unfortunately the number of days in a week may change so we
            # cannot hard-code literal 7. Boohoo.
            weekday = DAYS[(now.weekday() + col) % len(DAYS)]
            date = str((now + timedelta(days=col)).day)
            w = draw.textsize(weekday, font=self.font)[0]
            colmid = int(self.colw * 1.5) + col * (self.colw + 1)
            texty = self.vpad + 25 * (self.vpad + self.fontsize)
            draw.text((colmid - w // 2,
                       texty - self.fontsize),
                      weekday,
                      font=self.font,
                      fill=self.fontcolor)
            w = draw.textsize(date, font=self.font)[0]
            draw.text((colmid - w // 2,
                       texty),
                      date,
                      font=self.font,
                      fill=self.fontcolor)

        # Draw the actual events
        for evt in self.cal.events:
            self.draw_event(draw, evt)

        # Draw where we are in the current time (we are always in the first
        # day, just a matter of finding where we are on that)
        daypercent = percent_of_day(now)
        draw.line((self.colw + 1,
                   self.vpad +
                   int(daypercent * self.total_space) + self.fontsize // 2,
                   self.colw * 2,
                   self.vpad +
                   int(daypercent * self.total_space) + self.fontsize // 2),
                  fill=self.mecolor,
                  width=1)

    def draw_event(self, draw: ImageDraw, evt):
        today = datetime.today().replace(hour=0, minute=0, second=0)
        start = get_time(evt['start'])
        end = get_time(evt['end'])
        start = start.replace(tzinfo=None)
        end = end.replace(tzinfo=None)
        name = evt['summary']

        # For the case where an event spans multiple days
        duration = end - start
        fullday = duration >= timedelta(days=1)
        week_cutoff = timedelta(days=6, hours=23, minutes=59, seconds=59)
        while duration > timedelta(0) and start - today < week_cutoff:
            # Only draw the event if it is not a long time ago
            if start - today >= -timedelta(seconds=1):
                col = (start.weekday() - today.weekday()) % 7
                x0 = self.colw + col * (self.colw + 1) + 1
                x1 = self.colw + (col + 1) * (self.colw + 1) - 1
                if not fullday:
                    start_percent = percent_of_day(start)
                    end_percent = percent_of_day(end)
                    y0 = self.vpad +\
                        int(start_percent * self.total_space) + self.fontsize // 2
                    y1 = self.vpad +\
                        int(end_percent * self.total_space) + self.fontsize // 2
                else:
                    y0 = self.vpad + self.fontsize // 2
                    y1 = self.vpad + self.fontsize * 3

                # Draw event rectangle
                fill = self.evtcolor if not fullday else self.fulldaycolor
                draw.rectangle((x0, y0, x1, y1),
                               fill=fill)
                # Draw event text
                size = (x1 - x0, y1 - y0)
                fullday_needs_end = fullday and end.day != start.day
                slot = '%02d:%02d-%02d:%02d' % (
                    start.hour,
                    start.minute,
                    23 if fullday_needs_end else end.hour,
                    59 if fullday_needs_end else end.minute)
                draw.multiline_text((x0 + self.hpad, y0 + self.hpad),
                                    self.find_fit_text(draw, name, slot, size),
                                    fill=self.fontcolor,
                                    font=self.font)

            start += timedelta(days=1)
            start = start.replace(hour=0, minute=0, second=0)
            duration = end - start

    def save(self, filename: str):
        temp = Image.new('RGBA', self.size, (0, 0, 0, 0))
        temp.paste(self.panel,
                   (int(self.size[0] * 0.125), int(self.size[1] * 0.0833)))
        img = Image.alpha_composite(self.im, temp)
        with open(filename, 'wb') as f:
            img.save(f, 'PNG')

    def find_fit_text(self,
                      draw: ImageDraw,
                      text: str,
                      timeslot: str,
                      size: Tuple[int, int]) -> str:
        # Use font height as width
        formatter = GreedyFormatter(
            (size[0] - self.vpad * 2) // (self.fontsize // 3 + 1))
        text = formatter.format(text) + '\n' + timeslot
        h = draw.textsize(text, font=self.font, spacing=1)[1]
        # Optimize the height
        while not size[1] - 6 >= h and len(text.split('\n')) > 2:
            s = text.split('\n')
            text = '\n'.join(s[:-2] + [s[-1]])
            h = draw.textsize(text, font=self.font, spacing=self.vpad)[1]
        return text


