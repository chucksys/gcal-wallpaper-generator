from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
import pickle
from datetime import datetime, timedelta
import os
import logging


SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']


class GoogleCalendar:
    def __init__(self, args, env, cache):
        self.events = cache.get_events()
        self.got_from_cache = self.events is not None
        self.env = env
        self.args = args

        if not self.got_from_cache or self.args.refresh_cache:
            self.events = self.get_events_from_google()
            cache.events = self.events

        logging.info(f'Found {len(self.events)} events for the coming 7 days')

    def get_credentials(self, filename):
        creds = None
        if os.path.exists(filename):
            with open(filename, 'rb') as f:
                creds = pickle.load(f)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    self.args.creds_file, SCOPES)
                creds = flow.run_local_server(port=0)

            with open(filename, 'wb') as f:
                logging.info('Dumping credentials in file')
                pickle.dump(creds, f)

        return creds

    def get_events_from_google(self):
        logging.info('No event cache. Attempting to authenticate')
        creds = self.get_credentials(self.args.token_file)
        self.calendar = build('calendar', 'v3', credentials=creds,
                              cache_discovery=False)
        nowd = datetime.now().replace(hour=0, minute=0, second=0)
        now = nowd.isoformat() + 'Z'
        after7days = (nowd + timedelta(days=8)).isoformat() + 'Z'

        return self.calendar.events().list(
            calendarId=self.env['CALENDAR_ID'],
            timeMin=now,
            timeMax=after7days,
            maxResults=30,
            singleEvents=True,
            orderBy='startTime'
        ).execute().get('items', [])
