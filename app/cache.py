from datetime import date
import pickle
import os


CACHE_FILE = 'cache'


class FileCache:
    def __init__(self, env):
        backup = os.path.join(env.get('HOME', ''), '.cache')
        self.cache_dir = env.get('XDG_CACHE_HOME', backup)
        self.cache_dir = os.path.join(self.cache_dir, 'gcal-gen')
        self.cache_file = os.path.join(self.cache_dir, CACHE_FILE)

        if not os.path.exists(self.cache_dir):
            os.mkdir(self.cache_dir)

        self.populate()

    def populate(self):
        """Populate in-memory representation."""
        if not os.path.exists(self.cache_file):
            self.imagename = None
            self.day = None
            self.events = []
        else:
            with open(self.cache_file, 'rb') as f:
                obj = pickle.load(f)
                self.imagename = obj.get('imagename', None)
                self.day = obj.get('day', None)
                self.events = obj.get('events', [])

    def get_events(self):
        """Return events object (from pickle) if exists, otherwise None."""
        day = date.today().isoformat()
        if self.day == day:
            return self.events
        else:
            return None

    def cache(self):
        """Cache all objects for future use."""
        self.day = date.today().isoformat()
        obj = {
            'day': self.day,
            'imagename': self.imagename,
            'events': self.events
        }

        with open(self.cache_file, 'wb') as f:
            pickle.dump(obj, f)
