#!/usr/bin/bash
script_dir=$(dirname $0)

cd $script_dir
output_file=$(mktemp -u)
pipenv run gen -o $output_file $1
feh --bg-fill $output_file
rm $output_file
